#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#created by Stephan Fuchs (FG13, RKI), 2015
version = '0.0.9'

import subprocess
import os

class versions():
	'''
	Some functions to get some program versions
	''' 
	
	
	@staticmethod
	def bwaVer(cmd = "bwa"):
		p = subprocess.Popen(cmd.split(" "), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		output, err = p.communicate()
		output = err.decode('utf-8')
		for line in output.split("\n"):
			fields = line.split(":")
			if fields[0] == "Version":
				return fields[1].strip()
		return "None"
		
	@staticmethod
	def samtoolsVer(cmd = "samtools"):
		p = subprocess.Popen(cmd.split(" "), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		output, err = p.communicate()
		output = err.decode('utf-8')
		for line in output.split("\n"):
			fields = line.split(":")
			if fields[0] == "Version":
				return fields[1].strip()
		return "None"	

	@staticmethod
	def varscanVer(cmd = "varscan"):
		p = subprocess.Popen(cmd.split(" "), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		output, err = p.communicate()
		output = err.decode('utf-8')
		for line in output.split("\n"):
			fields = line.split(" ")
			if fields[0] == "VarScan":
				return fields[1].strip()
		return "None"			
		
	@staticmethod
	def mafftVer(cmd = "mafft"):
		p = subprocess.Popen(cmd.split(" ") + ['-t'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		output, err = p.communicate()
		output = err.decode('utf-8')
		return output.strip()
		
	@staticmethod
	def trimmomaticVer(cmd = "/usr/share/java/trimmomatic-0.32.jar"):
		return cmd

