#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#created by Stephan Fuchs (FG13, RKI), 2015
version = '0.0.9'

import time
import sys
import os
import re

class output():
	'''
	Some functions to create, modify, or delete files & folders
	''' 
	
	@staticmethod
	def createDir(dirname = False, mustEmpty = False, addtimestmp = False, silent = False):
		''' 
		Creates folder or use empty folder.
		Existing folders which are not empty induce error message and exit. 
		Timestamp can be added to folder name. 
		Returns folder name.
		
		Input:
		dirname 	designated folder name
		addtimestmp	add timestamp (YYYY-MM-DD_hh-mm-ss) to folder name (True | False)
		silent		show no messages except errors (True | False)
		mustEmpty	dir must be empty
		
		Return:
		path to folder
		'''		
		
		#msg
		if not silent:
			print("preparing output folder ...")
		
		#folder name given
		if dirname and not addtimestmp:
			if not os.path.isdir(dirname):
				os.makedirs(dirname)
			elif mustEmpty == True and len(os.listdir(dirname)) != 0:
				sys.exit("ERROR: the output directory is not empty!")
		
		#folder name not given						
		else:
			dirpref = dirname
			err = 0
			while True:
				timecode = time.strftime("%Y-%m-%d_%H-%M-%S")
				dirname = dirpref + '_' + timecode 
				if os.path.isdir(dirname):
					err += 1
				else:
					break
				if err == 30:
					sys.exit("ERROR: standard folder names already existing")
		
			os.makedirs(dirname)
			
		#msg
		if not silent:
			print(" ", dirname)	
		return dirname
		
				
	@staticmethod
	def checkFilename(filename, ask = True):
		if not os.path.isfile(filename):
			return True
		elif ask:
			f = os.path.relpath(os.path.abspath(filename), os.getcwd())
			i = 0
			while True:
				i += 1
				userinput = str(input(f + " exists! Should it be overwritten? [Y/n]:  "))
				if userinput == 'Y':
					return True
				elif userinput == 'n' or i == 5:
					return False			
		
		

				
	@staticmethod
	def createFile(filename, mode = 'w', owr = False, ask= True, silent = False):
		''' 
		creates files using given mode
		if file already exists, user can be asked for overwriting
		
		Input:
		filename	desginated file name
		mode		opening more
		owr			overwriting allowed
		ask			ask for user input before overwriting
		
		Return:
		file handle
		'''		
		
		#msg
		if not silent:
			print("preparing output file...")
		
		#file not existent or overwriting active
		if owr == False and output.checkFilename(filename, ask = ask) == False:
			sys.exit('ERROR: Output file exists! Program terminated.')
			
		#msg
		if not silent:
			print(" ", filename)		
				
		return open(filename, mode)
		
	@staticmethod
	def logging(line, filename, lb = "\n", mode="a"):
		'''
		writes text to file.
		By default file is opened using 'a' mode

		
		Input:
		line		text to write
		filename	file to be appended
		lb			line break
		mode		file opening mode
				
		Return:
		True
		'''	
		
		with open(filename, 'a') as handle:	
			handle.write(line + lb)	
			
		return True
		
	@staticmethod
	def formatAsCols (input, sep = ' ' * 5, lb = "\n"):
		'''
		formats list of lists in columns.
		List contains lines as lists containing columns as elements.
		'''
		
		
		#allowed signs in numbers
		pattern = re.compile(r'([+-])?([0-9]+)([.,]?)([0-9]+)(%?)')

		#str conversion
		lines = [[str(x) for x in line] for line in input] 

		#fill up rows to same number of columns (important for transposing)
		maxlen = max([len(x) for x in lines])
		[x.extend(['']*(maxlen-len(x))) for x in lines]    
		
		#find format parameters
		width = [] #colum width or length
		align = [] #alignment type (number = r; strings = l)
		prec = [] #number precision
		for column in zip(*lines):
			width.append(len(column[0]))
			prec.append(0)
			align.append('r')
			for field in column[1:]:
				if align[-1] ==  'l':
					width[-1] = max(width[-1], len(field))
				else:
					match = pattern.match(field)
					if match and match.group(0) == field:
						if match.group(3) and match.group(4):
							prec[-1] = max(prec[-1], len(match.group(4)))
						
						if prec[-1] > 0:
							k = 1
						else:
							k = 0
						width[-1] = max(width[-1], len(match.group(2)) + prec[-1] + k + len(match.group(4)))
					else:
						align[-1] = 'l'
						prec[-1] = False
						width[-1] = max(width[-1], len(field))
				
		#formatting
		output = []
		for	line in lines:
			f = 0
			output.append([])
			for field in line:
				if align[f] == 'l':
					output[-1].append(field + ' ' * (width[f] - len(field)))
				elif align[f] == 'r':
					match = pattern.match(field)
					if not match or match.group(0) != field: 
						output[-1].append(' ' * (width[f] - len(field)) + field)
					else:
						if match.group(5):
							percent = match.group(5)
						else:
							percent = ''
						length = len(percent)
						
						intpart = match.group(2)
						if match.group(1):
							intpart = match.group(1) + intpart
						if match.group(3):
							decpart = match.group(4) 
						else:
							intpart += match.group(4)
							decpart = ''							
						length += len(intpart)
						
						if prec[f] > 0:
							decpart += '0' * (prec[f] - len(match.group(4)))
							length += len(decpart) + 1
							formatted_number = ' ' * (width[f] - length) + intpart + '.' + decpart + percent
						else:
							formatted_number = ' ' * (width[f] - length) + intpart + percent
						output[-1].append(formatted_number)		
				f += 1
		
		return lb.join([sep.join(x) for x in output])
		
if __name__ == "__main__":
	print("version " + version)
