#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#created by Stephan Fuchs (FG13, RKI), 2017

import time
from myOutput import output
import myNgs 
from myVersions import versions
import argparse 
import shutil
import os
import sys
import subprocess

version = '2.0.0'

#timer
start_time = time.time()
benchmarks = []

#config - moudle list
module_inputs = []
module_inputs.append("fastq file consistency check (input: FASTQ)")
module_inputs.append("read trimming (input: FASTQ)")
module_inputs.append("read mapping (input: FASTQ)")
module_inputs.append("pileup (input: BAM)")
module_inputs.append("variant calling (input: PILEUP)")
module_inputs.append("consensus generation (input: VCF 4.1)")
module_index = []
i = 0
for module in module_inputs:
	module_index.append (str(i) + " - " + module)
	i += 1
	
# SELFTEST #
class SelftestAction(argparse.Action):
	def __call__(self, parser, namespace, values, option_string=None):
		try:
			print ("performing selftest ...")
			import doctest
			import unittest
			modules = ['myNgs']
			testSuite = unittest.TestSuite()  
			for module in modules:
				testSuite.addTest(doctest.DocTestSuite(module))
			print("creating test environment in folder", myNgs.test.getTestDir(), "...")	
			output.createDir(myNgs.test.getTestDir(), silent=True)		
			unittest.TextTestRunner(verbosity = 2).run(testSuite)
			print("deleting test environment", "...")
		finally:
			shutil.rmtree(myNgs.test.getTestDir())
		exit()	
		
def formatTime(starttime, endtime=time.time()):
	m, s = divmod(endtime - starttime, 60)
	h, m = divmod(m, 60)
	s = int(s)
	return "%02dh %02dm %02ds" % (h, m, s)

# definition of cli arguments #
parser = argparse.ArgumentParser(prog='batchMap', formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('reffile', metavar="REF_FILE", help="FASTA file containing reference sequence", type=argparse.FileType('r'))
parser.add_argument('files', metavar="FILE", help="input file(s)", type=argparse.FileType('r'), nargs="+")

generalopt = parser.add_argument_group("general options")
generalopt.add_argument('-t', metavar="INT", help="number of cpus (default: 10)", type=int, default=10)
generalopt.add_argument('--startm', metavar="INT", help="number of module to start your analysis (default: 0):\n  " + "\n  ".join(module_index), type=int, default=0)
generalopt.add_argument('--endm', metavar="INT", help="number of module to end your analysis (default: " + str(len(module_index)-1) + ").\n  " + "\n  ".join(module_index), type=int, default=len(module_index)-1)
generalopt.add_argument('--selftest', help='unit-like test of essential pipeline functions', action=SelftestAction, nargs=0)
generalopt.add_argument('--nogz', help='avoid compression of generated PILEUP and VCF files.', action='store_true')
generalopt.add_argument('--version', action='version', version='%(prog)s ' + version)

inputopt = parser.add_argument_group("input options")
inputopt.add_argument('--single', help="treat FASTQ files as single read-data (default: paired-end read data).", action="store_true")

outputopt = parser.add_argument_group("output options")
outputopt.add_argument('--outdir', metavar="STR", help="use a user-defined output directory", type=str, default=False)

trimopt = parser.add_argument_group("TRIMMOMATIC options (TRIMMING MODULE)")
trimopt.add_argument('--trimpath', metavar="STR", help="path to trimmomatic (default: /usr/share/java/trimmomatic.jar). The path has not to contain any spaces!", type=str, default='/usr/share/java/trimmomatic.jar')
trimopt.add_argument('--ic', metavar="STR", help="ILLUMINACLIP parameter (default: off)", type=str, default=False)
trimopt.add_argument('--lead', metavar="INT", help="LEADING parameter (default: off)", type=int, default=False)
trimopt.add_argument('--trail', metavar="INT", help="TRAILING parameter (default: off)", type=int, default=False)
trimalgorithm = trimopt.add_mutually_exclusive_group()
trimalgorithm.add_argument('--sw', metavar="STR", help="SLIDINGWINDOW parameter (default: 4:15)", type=str, default='4:15')
trimalgorithm.add_argument('--mx', metavar="STR", help="MAXINFO parameter (default: off)", type=str, default=False)
trimopt.add_argument('--ml', metavar="INT", help="MINLEN parameter (default: 36)", type=int, default=36)
trimopt.add_argument('--phred', metavar="STR", help="base quality encoding (default: auto-detect)", choices=["phred33", "phred64"], default=False)

mapopt = parser.add_argument_group("BWA options (MAPPING MODULE)")
mapopt.add_argument('--bwapath', metavar="STR", help="path to bwa (default: bwa). The path has not to contain any spaces!", type=str, default='bwa')
mapopt.add_argument('--bwasw', help="use bwasw instead of bwamem", action='store_true')
mapopt.add_argument('--bwaparam', metavar="STR", help="use additional bwa parameter provided as string (default: off). String has to be enclosed in quotation marks.", type=str, default="")

pileuppopt = parser.add_argument_group("SAMTOOLS options (MAPPING & PILEUP MODULE)")
mapopt.add_argument('--stpath', metavar="STR", help="path to samtools (default: samtools). The path has not to contain any spaces!", type=str, default='samtools')
mapopt.add_argument('--mpuparam', metavar="STR", help="use additional samtools mpileup parameter provided as string (default: -B). String has to be enclosed in quotation marks.", type=str, default='-B')

vcpopt = parser.add_argument_group("VARSCAN options (VARIANT CALLING MODULE)")
vcpopt.add_argument('--vspath', metavar="STR", help="path to varscan (default: varscan). The path has not to contain any spaces!", type=str, default='varscan')
vcpopt.add_argument('--min-coverage', metavar="INT", help="minimum read depth at a position to make a call (default: 10)", type=int, default=10)
vcpopt.add_argument('--min-reads2', metavar="INT", help="minimum supporting reads at a position to call variants (default: 8)", type=int, default=8)
vcpopt.add_argument('--min-avg-qual', metavar="INT", help="minimum base quality at a position to count a read (default: 20)", type=int, default=20)
vcpopt.add_argument('--min-var-freq', metavar="FLOAT", help="minimum variant allele frequency threshold to call variants (default: 0.8)", type=float, default=0.8)
vcpopt.add_argument('--min-freq-for-hom', metavar="FLOAT", help="minimum frequency to call homozygote (default: 0.8)", type=float, default=0.8)
vcpopt.add_argument('--p-value', metavar="FLOAT", help="default p-value threshold for calling variants (default: 0.01)", type=float, default=0.01)
vcpopt.add_argument('--strand-filter', metavar="INT", help="ignore variants with greater than 90 percent support on one strand (default: 0)", type=int, default=0)

cnspopt = parser.add_argument_group("CONSENSUS GENERATION (VARIANT CALLING MODULE)")
cnspopt.add_argument('--mapath', metavar="STR", help="path to mafft (default: mafft). The path has not to contain any spaces!!", type=str, default='mafft')
cnspopt.add_argument('--cns-max-var-noise', metavar="FLOAT", help="maximal variant allele frequency threshold to use wild type alleles (default: 0.2)", type=float, default=0.2)
cnspopt.add_argument('--cns-min-var-freq', metavar="FLOAT", help="minimum variant allele frequency threshold to use variant alleles.\nHas to be greater than or equal to value of --min-var-freq (default: same value as --min-var-freq).", type=float, default=False)
cnspopt.add_argument('--cns-report-var-freq', metavar="FLOAT", help="minimum variant allele frequency to report (default: 0.75).", type=float, default=0.75)
#cnspopt.add_argument('--cns-report-freq', metavar="FLOAT", help="minimum variant allele frequency threshold to variant alleles to be reported. (default: 0.75).", type=float, default=0.02)
cnspopt.add_argument('--cns-p-value', metavar="FLOAT", help="default p-value threshold for calling variants.\nHas to be smaller than or equal to value of --p-value (default: same value as --p-value).", type=float, default=False)
cnspopt.add_argument('--report', help="report consensus base call decisions (default: critical).", choices=['all', 'notref', 'variants', 'critical'], default="critical")
cnspopt.add_argument('--cns_inserts', metavar="INT", help="consider inserts when consensi are created (default: 1). Set 1 to activate or 0 to deactivate.", choices=[0, 1], default= 1, type=int)
cnspopt.add_argument('--algn_inserts', metavar="INT", help="consider inserts when consensi are created (default: 0). Set 1 to activate or 0 to deactivate. Different inserts at the same position are aligned using mafft.\nThis function is EXPERIMENTAL!", choices=[0, 1], type=int, default=0)

args = parser.parse_args()

# CHECK CLI PARAMETER #
piperange = range(args.startm, args.endm + 1)

if args.algn_inserts == 0:
	args.algn_inserts = False
else:
	args.algn_inserts = True

if args.cns_inserts == 0:
	args.cns_inserts = False
else:
	args.cns_inserts = True

if args.startm < 0:
	sys.exit("ERROR: parameter --startm cannot be smaller than 0")

if args.startm > len(module_index)-1:
	sys.exit("ERROR: parameter --startm cannot be greater than" + str(len(module_index)-1))
	
if args.endm < 0:
	sys.exit("ERROR: parameter --endm cannot be smaller than 0")

if args.endm > len(module_index)-1:
	sys.exit("ERROR: parameter --endm cannot be greater than" + str(len(module_index)-1))	
	
if args.endm < args.startm:
	sys.exit("ERROR: Sorry, but I cannot start with a module I am ending before.")

if args.cns_min_var_freq != False and args.min_var_freq > args.cns_min_var_freq and 4 in piperange:
	sys.exit("ERROR: --cns-min-var-freq has to be smaller than or equal to --min-var-freq.")
	
if args.cns_p_value is False:
	args.cns_p_value = args.p_value

if args.p_value < args.cns_p_value and 4 in piperange:
	sys.exit("ERROR: --cns-p-value has to be greater than or equal to --p-value.")	
	

def ls(dir):
	return[os.path.join(dir, f) for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))]
	
# START #
step = 0
dirs = {}
outfiles = {}

# PREPARE FILES & DIRS #
##outdir
if not args.outdir:
	dirs['main'] = output.createDir('BATCHMAP', addtimestmp = True)
else:
	dirs['main'] = output.createDir(args.outdir)
##tmpdir	
dirs['tmp'] = output.createDir(os.path.join(dirs['main'], "tmp"), silent=True)
##prepare process log file
outfiles['processlog'] = os.path.join(dirs['main'], "process.log")


# STEP 0: PROCESSING INPUT FILES
l = len(args.files)

##FASTQ input
if any([x <= 2 for x in piperange]):
	if args.startm == 0:
		check = True
	else:
		check = False
		
	i = 0
	raw_datasets = []
	while i < l:
		if args.single:
			print("processing FASTQ dataset", i+1, "of", l, "...")
			print(" ", os.path.basename(args.files[i].name))
			if check:
				print("  checking ...")			
			dataset = myNgs.fastqDataset(args.files[i].name, check=check)
			raw_datasets.append(dataset)
			if check:
				print("   ", dataset.file1.countReads(), "reads")
			i += 1
		else:
			print("processing FASTQ dataset", int(i/2)+1, "of", int(l/2), "...")
			print(" ", os.path.basename(args.files[i].name))
			print(" ", os.path.basename(args.files[i+1].name))
			if check:
				print("  checking ...")			
			dataset = myNgs.fastqDataset(args.files[i].name, args.files[i+1].name, check=check)
			raw_datasets.append(dataset)
			if check:
				print("   ", int(dataset.countReads()/2), "paired-end reads")
			i += 2
			
##BAM input
elif 3 in piperange:
	bamobjs = []
	i = 0
	while i < l:
		print("processing BAM file", i+1, "of", l, "...")
		bamfile = os.path.join(dirs['tmp'], os.path.basename(args.files[i].name))
		shutil.copy(args.files[i].name, bamfile)
		print(" ", os.path.basename(bamfile))	
		bamobjs.append(myNgs.bam(bamfile))
		i += 1
		
##PILEUP input
elif 4 in piperange:
	pileups = []
	i = 0
	while i < l:
		print("processing PILEUP file", i+1, "of", l, "...")
		pileupfile = os.path.join(dirs['tmp'], os.path.basename(args.files[i].name))
		shutil.copy(args.files[i].name, pileupfile)
		print(" ", os.path.basename(pileupfile))	
		pileups.append(myNgs.pileup(pileupfile))
		i += 1
		
##PILEUP input
elif 5 in piperange:
	vcfs = []
	i = 0
	while i < l:
		print("processing VCF file", i+1, "of", l, "...")
		pileupfile = os.path.join(dirs['tmp'], os.path.basename(args.files[i].name))
		shutil.copy(args.files[i].name, pileupfile)
		print(" ", os.path.basename(pileupfile))	
		vcfs.append(myNgs.vcf(pileupfile))
		i += 1	

benchmarks.append(('Processing input file', time.time()))


# STEP 1: PROCESSING REF
print("processing reference ...")
print("  " + args.reffile.name)
step += 1

##storing
dirname = str(step).zfill(2) + "_REF"
dirs['ref'] = output.createDir(os.path.join(dirs['main'], dirname), silent=True) 
ref = os.path.join(dirs['ref'], os.path.basename(args.reffile.name))
shutil.copy(args.reffile.name, ref)

##checking
print("  checking ...")
ref = myNgs.fasta(ref, silent=True)
if  ref.countEntries(silent=True) != 1:
	sys.exit("ERROR: FASTA file has to contain exactly one reference sequence.")

##indexing
if args.startm < 4:
	print("  indexing ...")
	if args.startm < 3:
		with open(outfiles['processlog'], "a") as handle:
			handle.write("#INDEXING USING BWA: " + ref.basename + "\n")
			handle.flush()
			ref.index("bwa", logfile=outfiles['processlog'], silent = True)
	if args.startm <= 3:	
		with open(outfiles['processlog'], "a") as handle:
			handle.write("#INDEXING USING SAMTOOLS: " + ref.basename + "\n")
			handle.flush()
			ref.index("samtools", logfile=outfiles['processlog'], silent = True)
			
	benchmarks.append(('Processing reference file', time.time()))			


# STEP2: TRIMMING
if 1 in piperange:
	print("trimming datasets ...	", end="")
	step += 1
	datasets = []
	l = len(raw_datasets)
		
	##storing
	dirname = str(step).zfill(2) + "_TRIMMING"
	dirs['trim'] = output.createDir(os.path.join(dirs['main'], dirname), silent=True) 

	##trimming
	for dataset in raw_datasets:
		print("\rtrimming datasets ... [" + str(len(datasets)) + "/" + str(l) + "]	", end="")
		with open(outfiles['processlog'], "a") as handle:
			handle.write("#TRIMMING: " + dataset.filestamp +"\n")
		objparam = {}
		objparam['outdir'] = dirs['trim']
		objparam['prog'] = args.trimpath
		objparam['ic'] = args.ic
		objparam['lead'] = args.lead
		objparam['trail'] = args.trail
		if 'sw' in args:
			objparam['sw'] = args.sw
			objparam['mx'] = False
		if 'mx' in args:
			objparam['sw'] = False
			objparam['mx'] = args.mx			
		objparam['ml'] = args.ml
		objparam['phred'] = args.phred
		objparam['logfile'] = outfiles['processlog']
		objparam['cpus'] = args.t
		objparam['silent'] = True		
		objparam['tophred'] = "TOPHRED33"
		obj = dataset.trimReads(**objparam)
		obj.file1.countReads(silent=True)	
		if args.single == False:		
			obj.file2.countReads(silent=True)
		datasets.append(obj)	
		
	print("\rtrimming datasets ... [" + str(len(datasets)) + "/" + str(l) + "]	", end="")
	print()
	
	benchmarks.append(('Trimming', time.time()))				
	
#entry (2)
elif args.startm == 2:
	datasets = raw_datasets


# STEP3: MAPPING
if 2 in piperange:
	print("read mapping ...	", end="")
	step += 1
	bamobjs = [] 
	l = len(datasets)
	
	#storing
	dirname = str(step).zfill(2) + "_MAPPING"
	dirs['map'] = output.createDir(os.path.join(dirs['main'], dirname), silent=True) 

	for dataset in datasets:
		#mapping
		print("\rread mapping ... [" + str(len(bamobjs)) + "/" + str(l) + "]	", end="")		
		with open(outfiles['processlog'], "a") as handle:
			handle.write("#MAPPING: " + dataset.filestamp + " vs " + ref.basename + "\n")		
		objparam = {}
		objparam['indexed_ref'] = ref.absname
		objparam['outdir'] = dirs['map']
		objparam['prog'] = args.bwapath
		if args.bwasw:
			objparam['prog'] += " bwasw"
		else:
			objparam['prog'] += " mem"
		objparam['progparam'] = args.bwaparam	
		objparam['cpus'] = args.t
		objparam['tmpdir'] = dirs['tmp']
		objparam['logfile'] = outfiles['processlog']
		
		sam = dataset.mapReads(**objparam)

		#converting
		print("\rconverting ...				", end="")
		with open(outfiles['processlog'], "a") as handle:
			handle.write("#SAM2BAM: " + sam.basename + "\n")
		objparam = {}
		objparam['outdir'] = dirs['map']
		objparam['bamfilename'] = False
		objparam['prog'] = args.stpath
		objparam['logfile'] = outfiles['processlog']
		objparam['delSam'] = True
		objparam['cpus'] = args.t
		bamobjs.append(sam.toBam(**objparam))
	
	print("\rread mapping ... [" + str(len(bamobjs)) + "/" + str(l) + "]	", end="")	
	print()
	
	benchmarks.append(('Mapping', time.time()))

# STEP4: PILEUP
if 3 in piperange:
	print("pileup ...	", end="")
	step += 1
	pileups = []
	l = len(bamobjs)
	
	#storing
	dirname = str(step).zfill(2) + "_VCALLING"
	dirs['vc'] = output.createDir(os.path.join(dirs['main'], dirname), silent=True) 
	
	for bam in bamobjs:	
		#sort bam
		print("\rsorting bam file ... [" + str(len(pileups)) + "/" + str(l) + "]               ", end="")
		with open(outfiles['processlog'], "a") as handle:
			handle.write("#BAM SORTING: " + bam.basename + "\n")
		objparam = {}
		objparam['filename'] = False
		objparam['logfile'] = outfiles['processlog']			
		objparam['prog'] = args.stpath
		objparam['cpus'] = args.t			
		bam.sort(**objparam)
		bam.sorted = True
		
		#index bam
		print("\rindexing bam file ... [" + str(len(pileups)) + "/" + str(l) + "]               ", end="")
		with open(outfiles['processlog'], "a") as handle:
			handle.write("#BAM INDEXING: " + bam.basename + "\n")		
		objparam = {}
		objparam['logfile'] = outfiles['processlog']			
		objparam['prog'] = args.stpath
		bam.index()
		bam.indexed = True

		#pileup
		print("\rpileup ... [" + str(len(pileups)) + "/" + str(l) + "]               ", end="")	
		with open(outfiles['processlog'], "a") as handle:
			handle.write("#PILEUP: " + bam.basename + " vs " + ref.basename + "\n")		
		objparam = {}
		objparam['reference'] = ref.absname		
		objparam['outdir'] = dirs['vc']			
		objparam['prog'] = args.stpath			
		objparam['param'] = args.mpuparam		
		objparam['logfile'] = outfiles['processlog']			
		objparam['cpus'] = args.t
		objparam['filename'] = False		
		pileups.append(bam.mpileup(**objparam))
		
	print("\rpileup ... [" + str(len(pileups)) + "/" + str(l) + "]               ", end="")	
	print()
	
	benchmarks.append(('Pileup', time.time()))
	
# STEP5: VARCALLING
if 4 in piperange:
	print("\rvariant calling ... ", end="")
	vcfs = []
	l = len(pileups)
	
	#storing
	if "vc" not in dirs:
		step += 1	
		dirname = str(step).zfill(2) + "_VCALLING"
		dirs['vc'] = output.createDir(os.path.join(dirs['main'], dirname), silent=True) 
	
	objparam = {}
	objparam['outdir'] = dirs['vc']
	objparam['mincov'] = args.min_coverage
	objparam['minreads2'] = args.min_reads2
	objparam['minavgqual'] = args.min_avg_qual
	objparam['minvarfreq'] = args.min_var_freq
	objparam['minhomfreq'] = args.min_freq_for_hom
	objparam['pval'] = args.p_value
	objparam['strandfilter'] = args.strand_filter
	objparam['prog'] = args.vspath
	objparam['filename'] = False
	objparam['logfile'] = outfiles['processlog']
	objparam['cpus'] = args.t	
		
	for pileup in pileups:
		print("\rvariant calling ... [" + str(len(vcfs)) + "/" + str(l) + "]	", end="")
		with open(outfiles['processlog'], "a") as handle:
			handle.write("#VARIANT CALLING: " + pileup.basename + "\n")				
		vcfs.append(pileup.callVar(**objparam))
		
	print("\rvariant calling ... [" + str(len(vcfs)) + "/" + str(l) + "]	", end="")
	print()
	
	benchmarks.append(('Variant calling', time.time()))

	
# STEP6: CONSENSUS GENERATION
if 5 in piperange:
	print("generating consensi ...", end="")
	cnsCollection = myNgs.cnsCollector()
	l = len(vcfs)
	step += 1
	
	#storing
	dirname = str(step).zfill(2) + "_CONSENSI"
	dirs['cns'] = output.createDir(os.path.join(dirs['main'], dirname), silent=True) 	
	
	
	#generating cns
	objparam = {}
	objparam['reference'] = ref.absname
	objparam['varfreq_excludewt'] = args.cns_max_var_noise
	objparam['minvarfreq'] = args.cns_min_var_freq
	objparam['warnthresh'] = args.cns_report_var_freq
	objparam['pvalthresh'] = args.cns_p_value
	objparam['silent'] = True
	if args.report == "none":
		objparam['report'] = False
	else:
		objparam['report'] = args.report
	
	i = 0
	for vcf in vcfs:
		print("\rgenerating consensi ... [" + str(i) + "/" + str(l) + "]	", end="")
		if args.startm < 5 and pileups:
			objparam['pileupfile'] = pileups[i].absname
		objparam['logfile'] = os.path.join(dirs['cns'], os.path.splitext(vcf.basename)[0] + "_cns.log")
		cns = vcf.createConsensi(**objparam)
		cnsCollection.add(*cns)	
		i += 1
	print("\rgenerating consensi ... [" + str(i) + "/" + str(l) + "]	", end="")
	print()
	print("writing consensi ...")
	for header in cnsCollection.iterHeader():
		if header.endswith(".vcf"):
			fheader = header[:-4]
		cnsfile = os.path.join(dirs['cns'], fheader + "_cns.fna") 
		with open(cnsfile, "w") as handle:
			handle.write(">" + fheader + "\n" + cnsCollection.getCns(header=header, inserts=args.cns_inserts))
			
	benchmarks.append(('Consensus generation', time.time()))

	#alignment
	print("aligning consensi ...")
	objparam = {}
	objparam['tmpdir'] = dirs['tmp']
	objparam['inserts'] = args.algn_inserts
	objparam['prog'] = args.mapath
	cnsCollection.align(**objparam)
	
	benchmarks.append(('Aligning consensus sequences', time.time()))
	
	print("writing alignment ...")
	algnfile = os.path.join(dirs['cns'], "alignment.fna") 
	with open(algnfile, "w") as handle:
		handle.write(cnsCollection.showAlign(format="fasta", inserts=args.algn_inserts))
		handle.write("\n")
		
	benchmarks.append(('Writing alignment file', time.time()))
		
	#inserts (has to be after alignment)
	if args.algn_inserts or args.cns_inserts:
		print("writing insert table ...")
		inserts = []
		for header in cnsCollection.iterHeader():
			for insert in cnsCollection.iterInserts(header):
				inserts.append([header] + list(insert))
		if len(inserts) > 0:
			inserts.sort(key=lambda x: x[4])
			insertfile = os.path.join(dirs['cns'], "inserts.csv") 
			with open(insertfile, "w") as handle:	
				handle.write("cns,insert sequence,aligned insert,position,position in alignment\n")
				for insert in inserts:
					insert = [str(x) for x in insert]
					handle.write(insert[0] + "," + insert[2] + "," + insert[1] + "," + insert[4] + "," + insert[3] + "\n")
					
		benchmarks.append(('Writing insert file', time.time()))
				

#STEP9: LOGGING
step += 1
print("logging ...")
#benchmarks.append(('Logging', time.time()))
logfile = os.path.join(dirs['main'], "batchMap.log") 

#dataset size (1 | 2) to format number of lines per dataset
if args.startm <= 2 and args.single == False:
	ds_size = 2
else:
	ds_size = 1
	
with open(logfile, "w") as handle:
	#batchMap general
	handle.write("BATCHMAP\n")
	handle.write("======================================\n")
	out = []
	out.append(['batchMap version', version])
	out.append(['trimmomatic version', versions.trimmomaticVer(args.trimpath)])
	out.append(['bwa version', versions.bwaVer(args.bwapath)])
	out.append(['samtools version', versions.samtoolsVer(args.bwapath)])
	out.append(['varscan version', versions.varscanVer(args.bwapath)])
	out.append(['mafft version', versions.mafftVer(args.bwapath)])
	handle.write(output.formatAsCols(out))
	benchmarks.append(('Logging general section', time.time()))

	#workflow
	handle.write("\n\nWORKFLOW\n")
	handle.write("======================================\n")
	out = []
	out.append(["#", "module", "status"])
	i = 0
	for module in module_inputs:
		if not i in piperange:
			r = "skipped"
		else:
			r = "included"
		out.append([str(i) + ".", module, r])
		i += 1
	handle.write(output.formatAsCols(out))
	benchmarks.append(('Logging workflow section', time.time()))

	#parameter
	handle.write("\n\nPARAMETER\n")
	handle.write("======================================\n")	
	i = 0
	j = 0
	for i in piperange:
		#trimming parameter
		if i == 1:
			j += 1
			handle.write(str(j) + ". read trimming\n")
			out = []
			out.append(["program:", "trimmomatic"])
			out.append(["ILLUMINACLIP:", args.ic])
			out.append(["LEAD:", args.lead])
			out.append(["TRAIL:", args.lead])
			if args.sw:
				out.append(["SLIDINGWINDOW:", args.sw])
			else:
				out.append(["MAXINFO:", args.mx])
			out.append(["MINLEN:", args.ml])
			if args.phred:
				out.append(["PHRED:", args.phred])
			else:
				out.append(["PHRED:", "auto-detect"])
			out.append(["TOPHRED33:", "yes"])
			handle.write(output.formatAsCols(out))
		
		#mapping parameter
		elif i == 2:
			j += 1
			handle.write(str(j) + ". read mapping\n")
			out = []
			if args.bwasw:
				out.append(["algorithm:", "bwasw"])
			else:
				out.append(["algorithm:", "bwa mem"])
			if args.bwaparam != "":
				out.append(["bwa parameter:", "'" + args.bwaparam + "'"])
			else:
				out.append(["bwa parameter:", "default"])
			handle.write(output.formatAsCols(out))				
		
		#pileup parameter
		elif i == 3:
			j += 1
			handle.write(str(j) + ". pileup\n")
			out = []		
			out.append(["algorithm:", "samtools mpileup"])
			if args.mpuparam != "":				
				out.append(["parameter:", "'" + args.mpuparam + "'"])
			else:
				out.append(["parameter:", "default"])				
			handle.write(output.formatAsCols(out))
		
		#variant calling parameter		
		elif i == 4:
			j += 1
			handle.write(str(j) + ". variant calling\n")
			out = []		
			out.append(["program:", "varscan"])
			out.append(["min-coverage:", args.min_coverage])						
			out.append(["min-reads2:", args.min_reads2])						
			out.append(["min-avg-qual:", args.min_avg_qual])						
			out.append(["min-var-freq:", args.min_var_freq])						
			out.append(["min-freq-for-hom:", args.min_freq_for_hom])						
			out.append(["p-value:", args.p_value])						
			out.append(["strand_filter:", args.strand_filter])						
			handle.write(output.formatAsCols(out))	
			
		#consensus generation
		elif i == 5:
			j += 1
			handle.write(str(j) + ". consensus creation\n")
			out = []		
			out.append(["program:", "batchMap"])
			out.append(["cns-min-var-freq:", args.cns_min_var_freq])
			out.append(["cns-max-var-noise:", args.cns_max_var_noise])
			out.append(["cns-report-var-freq:", args.cns_report_var_freq])
			out.append(["cns-p-value:", args.cns_p_value])
			out.append(["reporting:", args.report])

			if args.cns_inserts:
				out.append(["inserts in consensi:", "included"])						
				out.append(["mafft parameter:", "'--localpair --maxiterate 1000'"])
			else:
				out.append(["inserts in consensi:", "excluded"])		
				
			if args.algn_inserts:
				out.append(["inserts in alignment:", "included"])						
				out.append(["inserts in alignment aligned using:", "mafft"])						
				out.append(["mafft parameter:", "'--localpair --maxiterate 1000'"])
			else:
				out.append(["inserts in alignment:", "excluded"])		
			handle.write(output.formatAsCols(out))

		if i+1 in piperange:
			handle.write("\n\n")
	benchmarks.append(('Logging parameter section', time.time()))
		
	#datasets
	handle.write("\n\nINPUT & OUTPUT\n")
	handle.write("======================================\n")
	
	#raw FASTQ datasets
	out = []
	out.append(["dataset"])
	
	#fill rows with dataset number
	j = 0
	if args.startm <= 2:
		for dataset in datasets:
			j += 1
			out.append([j])
			
			if ds_size == 2:
				out.append([j])
	
	elif args.startm == 3:
		for j in range(len(bamobjs)):
			j += 1
			out.append([j])

	elif args.startm == 4:
		for j in range(len(pileups)):
			j += 1
			out.append([j])		

	elif args.startm == 5:
		for j in range(len(vcfs)):
			j += 1
			out.append([j])		
			
	
	if 0 in piperange or 1 in piperange or 2 in piperange:
		out[0].extend(['file (raw data)', 'type (raw data)', '#reads (raw data)'])
					
		j = 0
		for dataset in raw_datasets:
			j += 1
			if ds_size == 1:
				out[j].extend([j, dataset.file1.basename, 'FASTQ (single)', dataset.file1.countReads(silent=True)])
			else: 
				out[j].extend([dataset.file1.basename, 'FASTQ (paired-end; forward reads)', dataset.file1.countReads(silent=True)])
				j += 1
				out[j].extend([dataset.file2.basename, 'FASTQ (paired-end; reverse reads)', dataset.file2.countReads(silent=True)])
	
	#trimmed FASTQ datasets
	if 1 in piperange:
		out[0].extend(['file (trimmed data)', 'type (trimmed data)', '#reads (trimmed data)'])
		j = 0
		for dataset in datasets:
			j += 1
			if ds_size == 1:
				out[j].extend([dataset.file1.basename, 'FASTQ (single-end)', dataset.file1.countReads()])
			else: 
				out[j].extend([dataset.file1.basename, 'FASTQ (paired-end; forward reads)', dataset.file1.countReads(silent=True)])
				j += 1
				out[j].extend([dataset.file2.basename, 'FASTQ (paired-end; reverse reads)', dataset.file2.countReads(silent=True)])
				
							
	#BAM datasets
	if 2 in piperange or 3 in piperange:
		out[0].extend(['file (mapped data)', 'type (mapped data)', 'mapped reads (total)'])
		j = 0
		for dataset in bamobjs:
			j += 1
			if ds_size == 1:
				out[j].extend([dataset.basename, 'BAM', dataset.countMappedReads(prog="args.stpath")])
			else: 
				out[j].extend([dataset.basename, 'BAM', dataset.countMappedReads(prog="args.stpath")])
				j += 1
				out[j].extend([dataset.basename, 'BAM', dataset.countMappedReads(prog="args.stpath")])
				
	#pileup
	if 3 in piperange or 4 in piperange:
		out[0].extend(['file (pileup data)', 'type (pileup data)'])		
		j = 0
		for dataset in pileups:
			j += 1
			if ds_size == 1:
				out[j].extend([dataset.basename, 'PILEUP'])
			else: 
				out[j].extend([dataset.basename, 'PILEUP'])
				j += 1
				out[j].extend([dataset.basename, 'PILEUP'])
				
	#vcf
	if 4 in piperange or 5 in piperange:
		out[0].extend(['file (vcf data)', 'type (vcf data)'])		
		j = 0
		for dataset in vcfs:
			j += 1
			if ds_size == 1:
				out[j].extend([dataset.basename, 'VCF'])
			else: 
				out[j].extend([dataset.basename, 'VCF'])
				j += 1
				out[j].extend([dataset.basename, 'VCF'])	

	#consensus
	if 5 in piperange:
		out[0].extend(['file (consensus)', 'length (without gaps)', 'Ns', 'SNPs (without Ns)', 'deleted bases', 'gaps'])
		if args.cns_inserts or args.algn_inserts:
			j -= 1				
			out[0].extend(['inserted bases'])			
			out[0].extend(['inserts'])			
			out[0].extend(['file (inserts)'])			
		j = 0
		for dataset in vcfs:
			j += 1
			stats = cnsCollection.getStat(dataset.basename)
			length = stats['length']
			if not args.cns_inserts:
					length -= stats['inserted_bases']
		
			filename = dataset.basename		
			if filename.endswith(".vcf"):
				filename = filename[:-4]
			filename += "_cns.fna"
				
			if ds_size == 1:
				out[j].extend([filename])
				out[j].extend([length])
				out[j].extend([stats['N']])
				out[j].extend([stats['snps']])
				out[j].extend([stats['deleted_bases']])
				out[j].extend([stats['gaps']])
				
			else: 
				out[j].extend([filename])
				out[j].extend([length])
				out[j].extend([stats['N']])
				out[j].extend([stats['snps']])
				out[j].extend([stats['deleted_bases']])
				out[j].extend([stats['gaps']])
				j += 1
				out[j].extend([filename])
				out[j].extend([length])
				out[j].extend([stats['N']])
				out[j].extend([stats['snps']])
				out[j].extend([stats['deleted_bases']])
				out[j].extend([stats['gaps']])		
			
			#inserts
			if args.cns_inserts:
				j -= 1				
				out[0].extend(['inserted bases'])			
				out[0].extend(['inserts'])			
				out[0].extend(['file (inserts)'])					
			
				if ds_size == 1:
					out[j].extend([stats['inserted_bases']])
					out[j].extend([stats['inserts']])
					out[j].extend([filename])
					
				else: 
					out[j].extend([stats['inserted_bases']])
					out[j].extend([stats['inserts']])
					out[j].extend([filename])
					j += 1
					out[j].extend([stats['inserted_bases']])
					out[j].extend([stats['inserts']])		
					out[j].extend([filename])
	
	handle.write(output.formatAsCols(out))
	benchmarks.append(('Logging Input/Output section', time.time()))
	
	#benchmarks
	handle.write("\n\nBenchmarks\n")
	handle.write("======================================\n")	
	i = 0
	benchlog = []
	for benchmark in benchmarks:
		if i == 0:
			s = start_time
		else:
			s = benchmarks[i-1][1]
		benchlog.append([benchmark[0], formatTime(s, benchmark[1])])
		i += 1

		
#STEP10: CLEANING
if not args.nogz:
	print ("cleaning and compressing ...")
else:
	print ("cleaning ...")
	
#tmp dir 
shutil.rmtree(dirs['tmp'])	

#reference dir	
if 'ref' in dirs: 
	for filename in ls(dirs['ref']):
		if any([filename.endswith('.amb'), filename.endswith('.ann'), filename.endswith('.bwt'), filename.endswith('fai'), filename.endswith('pac'), filename.endswith('sa')]):
			try:
				os.remove(filename)
			except OSError:
				pass				
				
#bam dir
if 'map' in dirs:
	for filename in ls(dirs['map']):
		if filename.endswith('.bai'):
			try:
				os.remove(filename)
			except OSError:
				pass
	
#vcf dir
if 'vc' in dirs:
	for filename in ls(dirs['vc']):
		subprocess.check_call(['gzip', filename])
		
if not args.nogz:
	benchmarks.append(('cleaning and compressing files', time.time()))		
else:
	benchmarks.append(('cleaning', time.time()))		
	
#final logging
with open(logfile, "a") as handle:
	benchlog.append([benchmarks[-1][0], formatTime(benchmarks[-2][1],benchmarks[-1][1])])
	benchlog.append([])
	benchlog.append(['TOTAL', formatTime(start_time, time.time())])	
	handle.write(output.formatAsCols(benchlog))
