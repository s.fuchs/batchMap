#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#created by Stephan Fuchs (FG13, RKI), 2015
version = '0.0.9'

from myOutput import output
import argparse
import time
import sys
import os
import math
import subprocess
import tempfile
import shutil
import gzip
import random
import re
import hashlib


class file():
	''' 
	provides basic attributes to files
	'''
	
	def __init__(self, filename):	
		self.name = filename
		self.basename = self.getBasename(filename)
		self.absname = self.getAbsname(filename)		
		self.dir = self.getDir(filename)		
		
	def name(self):
		return self.name
	
	def getBasename(self, filename):	
		return os.path.basename(filename)
		
	def getAbsname(self, filename):	
		return os.path.abspath(filename)
		
	def getDir(self, filename):
		return os.path.dirname(self.getAbsname(filename))

	def exists(self):
		return os.path.isFile(self.absname)
		
	def checkGz(self):
		'''
		checks if file is GZ compressed
		
		Return:
		True/False
		'''
		
		handle = gzip.GzipFile(self.absname, 'r')		
		gz = True
		try:
			handle.read(1)
		except:
			gz = False
		
		return gz

	def openFile(self, mode ="r"):
		'''
		opens plain/gz file using open() or gzip.GzipFile()
		
		Return:
		file handle
		'''
		
		if self.gzcompression:
			return gzip.GzipFile(self.absname, mode + "b")
		else:
			return open(self.absname, mode)		
	
	def rename(self, filename):
		self.__init__(filename)

		
class test():
	'''
	provides test data and files for doctests
	'''
	
	## GET TESTDATA ##
	
	@staticmethod
	def copyTestfile(fname):
		src = os.path.join(test.getTestdataDir(), fname)
		dst = os.path.join(test.getTestDir(), fname)		
		shutil.copyfile(src,dst)
		return dst
	
	@staticmethod
	def getTestDir():
		return ".BATCHMAP_SELFTEST"
		
	@staticmethod
	def cleanTestDir():
		for the_file in os.listdir(test.getTestDir()):
			file_path = os.path.join(test.getTestDir(), the_file)
			if os.path.isfile(file_path):
				os.unlink(file_path)
			elif os.path.isdir(file_path): shutil.rmtree(file_path)
			
	@staticmethod
	def getTestdataDir():
		return os.path.join(os.path.dirname(os.path.abspath(__file__)), "testdata")
		
	@staticmethod	
	def getTestFasta():
		name = "test.fasta"
		return test.copyTestfile(name)
		
	@staticmethod	
	def getTestRef():
		name = "testref.fasta"
		return test.copyTestfile(name)	

	@staticmethod	
	def getTestFastQ(paired = True):
		f1 = "test1.fastq.gz"
		if paired:
			f2 = "test2.fastq.gz"
			return (test.copyTestfile(f1), test.copyTestfile(f2))
		else:
			return test.copyTestfile(f1)			
			
	@staticmethod	
	def getTestSam():
		name = "test.sam.gz"
		src = os.path.join(test.getTestdataDir(), name)
		dst = os.path.join(test.getTestDir(), name[:-3])	
		with gzip.open(src, 'rb') as f_in, open(dst, 'wb') as f_out:
			shutil.copyfileobj(f_in, f_out)
		return dst
		
	@staticmethod	
	def getTestBam():
		name = "test.bam"
		return test.copyTestfile(name)			

	@staticmethod	
	def getTestPileup():
		name = "test.pileup.gz"
		src = os.path.join(test.getTestdataDir(), name)
		dst = os.path.join(test.getTestDir(), name[:-3])	
		with gzip.open(src, 'rb') as f_in, open(dst, 'wb') as f_out:
			shutil.copyfileobj(f_in, f_out)
		return dst			
		
	@staticmethod	
	def getTestVcf():
		name = "test.vcf.gz"
		src = os.path.join(test.getTestdataDir(), name)
		dst = os.path.join(test.getTestDir(), name[:-3])	
		with gzip.open(src, 'rb') as f_in, open(dst, 'wb') as f_out:
			shutil.copyfileobj(f_in, f_out)
		return dst	


class fasta(file):
	def __init__(self, filename, silent=False):	
		super().__init__(filename)
		self.count = False		
			
	def index(self, tool, logfile = False, silent = False):
		if not silent:
			print("indexing ", self.basename, "...")			
			
		if tool == 'bwa':
			cmd = ['bwa', 'index', self.absname]
		elif tool == 'samtools':
			cmd = ['samtools', 'faidx', self.absname]
		else:
			sys.exit('ERROR: unknown tool')
			
		if not logfile:
			subprocess.check_call(cmd, shell=False)
		else:
			with open(logfile, "a") as loghandle:
				subprocess.check_call(cmd, stdout=loghandle, stderr=loghandle)			

	@staticmethod	
	def readFasta(filename, silent = False):
		'''
		creates a generator returning tuples of (header, sequence) obtained from a FASTA file. All lines are stripped.

		Input:
		filename	name of the FASTA file with one or more entries
		silent		show no messages (True | False)
		
		Return:
		generator of tuples (stripped header, stripped sequence) for each entry in the FASTA file
		
		Doctest:
		tested in fasta.countEntries()	
		
		'''
		
		if not silent:
			print("processing", filename, "...")
		
		with open(filename, 'r') as handle:
			
			#find first header:
			for line in handle:
				line = line.strip()
				
				if line[0] == ">":
					header = line
					break
		
			#process file after first header
			seq = []
			for line in handle:
				line = line.strip()

				if line == "" or line[0] == ";":
					continue
					
				elif line[0] == ">":
					entry = (header, "".join(seq))
					header = line
					seq = []
					yield entry
				
				else:
					seq.append(line)
					
		entry = (header, "".join(seq))
		yield entry		

	@staticmethod	
	def toDict(filename, silent = False):
		dict = {}
		for header, seq in fasta.readFasta(filename, silent=silent):
			if header in dict:
				sys.exit("ERROR: header not unique in " + os.path.basename(filename))
			else:
				dict[header] = seq
		return dict
		
	def countEntries(self, force = False, silent=False):
		'''
		Doctest:
		>>> fastafile = test.getTestFasta()
		>>> obj = fasta(fastafile, silent=True)
		>>> obj.countEntries(fastafile, silent=True)
		4
		>>> print(sorted(list(obj.toDict(fastafile, silent=True).values())))
		['ATCGATT', 'ATTCGAATT', 'ATTCGATT', 'ATTCGATT']
		>>> test.cleanTestDir()
						
		'''	
		if self.count == False or force == True:
			i = 0
			for entry in self.readFasta(self.absname, silent=silent):
				i += 1
			self.count = i
		
		return self.count
		
	@staticmethod
	def mafft(filename, prog="mafft", cpus = 1, logfile = False, silent = False):
		'''
		generates a mpileup file using samtools
		
		Parameter:
		filename	name of input FASTA file containing sequences to align [file]
		logfile		logfile name for stderr
		cpus		number of cpus
		silent		show no messages except error messages
		
		Return:
		
		Doctest:
		>>> fastafile = test.getTestFasta()
		>>> logfile = os.path.join(test.getTestDir(), "consensus.log")
		>>> fasta.mafft(fastafile, silent=True, logfile=logfile)
		['>seq1', 'attcgatt-', '>seq2', 'attcgatt-', '>seq3', 'attcgaatt', '>seq4', 'a-tcgatt-']
		>>> test.cleanTestDir()
						
		'''
		
		if not silent:
			print("aligning ", filename, "...")			

		cmd = prog.split(" ") + ['--localpair', '--maxiterate', '1000',  filename]
		
		if not logfile:
			out = list(filter(None, subprocess.check_output(cmd).decode('ascii').split("\n")))
		else:
			with open(logfile, "a") as loghandle:
				out = list(filter(None, subprocess.check_output(cmd, stderr=loghandle).decode('ascii').split("\n")))
		return out

		
class fastq(file):
	'''
	fastq file object
	'''
	
	def __init__(self, filename, check=True, silent=False):	
		super().__init__(filename)
		self.gzcompression = self.checkGz()
		self.readcount = False
		if check:
			self.countReads(silent=silent)			
			
	
	def __enter__(self):
		return self					
	
	def readFastq(self, linestripping = False, silent = False):
		'''
		creates a generator returning tuples of (identifier, description, read sequence, base quality scores) obtained from a FASTQ file.
		
		Exit is provoked by FASTQ format errors
		
		Input:
		filename		name of the FASTQ file
		linestripping	strip chars from both ends of lines (str)
						or False to not perform line stripping
		silent			show no messages (True | False)
		
		Return:
		generator of tuples (identifier, description, read sequence, base quality scores) for each read in the FASTQ file		
		
		Doctest:
		this function is tested in countReads function
		
		'''
		
		#opening
		if not silent:
			print("processing", self.basename, "...")	
		
		with self.openFile() as handle:
			#reading
			e = 0
			l = 0
			for line in handle:
				l += 1 #counts line number
				e += 1 #counts line until 4 then starts at 1 to check entry lines
				
				if self.gzcompression:
					line = line.decode('utf-8')		

				#identifier
				if e == 1:
					if line.strip() == "":
						e -= 1
						continue
					elif line[0] != "@":
						sys.exit("ERROR: " + filename + "is not a valid FASTQ file (identifier expected at line " + str(l) + ")")
				
					if linestripping:
						id = line.strip(linestripping)
						
					else:
						id = line
				
				#sequence
				elif e == 2:
					if line.strip() == "":	
						sys.exit("ERROR: " + handle.name + "is not a valid FASTQ file (sequence expected at line " + str(l) + ")")			

					if linestripping:
						seq = line.strip(linestripping)
					else:
						seq = line
						
				#description
				elif e == 3:
					if line[0] != "+":
						sys.exit("ERROR: " + filename + "is not a valid FASTQ file (description expected at line " + str(l) + ")")
					
					if linestripping:
						descr = line.strip(linestripping)
					else:
						descr = line					

				#base qualities
				else:
					if line.strip() == "":	
						sys.exit("ERROR: " + filename + "is not a valid FASTQ file (quality scores expected at line " + str(l) + ")")
				
					else:		
						e = 0
					
						if linestripping:
							qual = line.strip(linestripping)
						else:
							qual = line					
					
						yield (id, seq, descr, qual)
					
			if l < 4 and l > 0:
				sys.exit("ERROR: " + filename + "is not a valid FASTQ file (truncated entry at line " + str(l) + ")")
	
	def countReads(self, force=False, silent=False):
		''' 
		returns number of reads in a fastqfile
		
		Parameter:
		force	force counting (do not use value in self.readcount) [True / False]
		silent	do not show any message except error messages [True / False]
		
		Return:
		number of reads [int]
		
		Doctest:		
		>>> fastqfile = test.getTestFastQ(paired=False)
		>>> obj = fastq(fastqfile, check=False, silent=True)
		>>> obj.countReads(silent=True)
		564270
		
		'''
		
		if force == True or self.readcount == False:			
			r = 0
			for readdata in self.readFastq(silent=silent):
				r += 1
			self.readcount = r
			
		return self.readcount
		
	
class fastqDataset():
	'''
	fastq dataset object
	
	'''
	
	def __init__(self, filename1, filename2=False, check=False, silent=True):
		self.file1 = fastq(filename1, check, silent)
		self.files = [self.file1]
		self.readcount = False
				
		if filename2:
			self.paired = True
			self.file2 = fastq(filename2, check, silent)
			self.files.append(self.file2)
			self.filestamp = self.file1.basename + " & " + self.file2.basename
			
		else:
			self.paired = False
			self.file2 = False
			self.filestamp = self.file1.basename

		if check:
			self.countReads(silent = silent)
				
	
	## GET/SET ATTRIBUTES ##	
	def isPaired(self):
		return self.paired

	def countReads(self, force=False, silent=False):
		''' 
		returns number of reads in a fastqfile
		
		Parameter:
		force	force counting (do not use value in self.readcount) [True / False]
		silent	do not show any message except error messages [True / False]
		
		Return:
		Number of reads [int]
		
		Doctest:
		This function is tested under mapReads
		
		'''
		
		if force == True or self.readcount == False:
			counts = []
			for fileobj in self.files:
				counts.append(fileobj.countReads(force, silent))
			
			if len(set(counts)) != 1:
				sys.exit("ERROR:" + self.file1.basename + " and " + self.file2.basename + " contain different numbers of reads.") 
			
			self.readcount = sum(counts)

		return self.readcount

	## ANALYSES ##
	### trimming ###
	def trimReads(self, outdir, prog = '/usr/share/java/trimmomatic.jar', ic=False, lead=False, trail=False, sw="4:15", mx=False, ml=36, phred=False, tophred=False, cpus=1, logfile = "trimming.log", silent=True):
		'''
		read trimming using Trimmomatic
		
		Parameter:
		prog		path and name to trimmomatic
		ic			corresponds to Trimmomatic's parameter ILLUMINACLIPPING
		lead		corresponds to Trimmomatic's parameter LEADING
		trail		corresponds to Trimmomatic's parameter TRAILING
		sw			corresponds to Trimmomatic's parameter SLIDINGWINDOW
		mx			corresponds to Trimmomatic's parameter MAXINFO
		ml			corresponds to Trimmomatic's parameter MINLEN
		phred		corresponds to Trimmomatic's parameter phred33 / phred64
		cpus		number of cpus
		path		path to save  results
		logfile		path and name of process logfile (STDERR)
		silent		no messages except error messages
		
		Return:
		new fastqDataset object [obj]
		
		Doctest:
		This function is tested under mapReads
		'''
		
		#prepare output file names and CMD
		outdir = os.path.abspath(outdir)
		
		if phred:
			phred = "-" + phred
		else:
			phred = ""
			
		if self.paired:
			filebase = os.path.commonprefix([self.files[0].basename, self.files[1].basename])
			f1P = os.path.join(outdir, filebase + "_1_paired.fq.gz")
			f1U = os.path.join(outdir, filebase + "_1_unpaired.fq.gz")
			f2P = os.path.join(outdir, filebase + "_2_paired.fq.gz")
			f2U = os.path.join(outdir, filebase + "_2_unpaired.fq.gz")
			output.checkFilename(f1P, ask = True)			
			cmd = ['java', '-jar', prog, 'PE', '-threads', str(cpus), phred, self.files[0].absname, self.files[1].absname, f1P, f1U, f2P, f2U]
		else:
			f = os.path.splitext(self.files[0].basename)[0] + "_trimmed.fq.gz"
			f = os.path.join(outdir, f)
			output.checkFilename(f, ask = True)	
			cmd = ['java', '-jar', prog, 'SE', '-threads', str(cpus), phred, self.files[0].absname, f]			
		
		#prepare TRIMMOMATIC parameter and add to CMD
		if ic:
			cmd.append("ILLUMINACLIP:" + illuminaclip)
		if lead:
			cmd.append("LEADING:" + str(lead))
		if trail:
			cmd.append("TRAILING:" + str(trail))			
		if sw:
			cmd.append("SLIDINGWINDOW:" + sw)
		elif mx:
			cmd.append("MAXINFO:" + mx)				
		if ml:
			cmd.append("MINLEN:" + str(ml))
			
		if tophred:
			tophred = "-" + tophred
		else:
			tophred = ""			
		
			
		#execute
		cmd = list(filter(None, cmd))
		with open(logfile, "a") as loghandle:	
			out = subprocess.check_call(cmd, shell=False, stdout=loghandle, stderr=loghandle) 
		
		#return obj
		if self.paired:
			return fastqDataset(f1P, f2P, silent=silent)
		else:
			return fastqDataset(f, silent=silent)
			
			
	### mapping ###
	def mapReads(self, indexed_ref, outdir, prog = "bwa mem", progparam = "", cpus=1, tmpdir=False, logfile = "process.log"):
		'''
		maps reads to a reference sequence using a given mapping tool
		
		Parameter:
		indexed_ref	name of indexed FASTA file containing reference sequence [filename]
		prog		command to execute mapping tool e.g. "bwa mem" or "bwa bwasw" (sam file output has to be STDOUT) [path-to-mapper]
		cpus		number of cpus [int]
		path		path to save results
		tmpdir		path to save temporary data
		logfile		path + name of process logfile
		silent		do not show any message except error messages [True | False]
		
		Return:
		sam file object [obj]
		
		Doctest:
		>>> fastqfiles = test.getTestFastQ()
		>>> obj = fastqDataset(fastqfiles[0], fastqfiles[1], check=False, silent=True)
		>>> outdir = test.getTestDir()
		>>> logfile = os.path.join(outdir, "process.log")
		>>> trimmedobj = obj.trimReads(outdir, phred="phred33", logfile=logfile, silent=True)
		>>> trimmedobj.countReads(silent='True')
		1128540
		>>> reffile = fasta(test.getTestRef())
		>>> reffile.index('bwa', silent=True, logfile=logfile)
		>>> samobj = trimmedobj.mapReads(reffile.absname, outdir, logfile=logfile)
		>>> test.cleanTestDir()
		'''
		
		#create paths
		outdir = os.path.abspath(outdir)
		if tmpdir == False or os.path.exists(tmpdir) == False:
			tmpdir = os.path.abspath(output.createDir(os.path.join(outdir, 'tmp'), silent = True))
		
		#set output file names
		if self.isPaired():
			filebase = os.path.commonprefix([self.file1.basename, self.file2.basename])
		else:
			filebase = os.path.splitext(self.file1.basename)[0]
		samfile = os.path.join(outdir, filebase + ".sam")
		output.checkFilename(samfile, ask = True)

		#build command & execute
		with open(samfile, "w") as samhandle, open(logfile, "a") as loghandle:
			cmd = prog.split(" ")
			if progparam != "":
				cmd.extend(progparam.split(" "))
			cmd.extend(['-t', str(cpus), indexed_ref, self.file1.absname])
			if self.isPaired():
				cmd.append(self.file2.absname)
			subprocess.check_call(cmd, stdout=samhandle, stderr=loghandle)						
		
		#return obj
		return sam(samfile)
		
	
class sam(file):
	
	def __init__(self, filename, silent=False):	
		super().__init__(filename)
		
		
	# sam2bam format conversion #
	def toBam(self, bamfilename = False, prog = "samtools", outdir = False, logfile = "process.log", cpus = 1, delSam = False):
		'''
		converts a SAM file to a sorted BAM file using samtools
		
		stdout is written to a given file (bamfilename)
		stderr is written to a given log file ('a' mode)
		
		Exit is provoked by samtools errors
		
		Input:
		samfilename	name of the SAM file
		bamfilename	name of the designated BAM file, will be overwritten!
		logfile		name of log file stderr of samtools is appended to
		cpus		number of cpus to use
		delSam		delete SAM file after conversion (True | False)
		
		Return:
		bam object [obj]
		
		Doctest:
		>>> samfile = test.getTestSam()
		>>> obj = sam(samfile)
		>>> outdir = test.getTestDir()
		>>> logfile = os.path.join(outdir, "process.log")
		>>> bamobj = obj.toBam(outdir=outdir, logfile=logfile)
		>>> test.cleanTestDir()
					
		'''
		
		#prepare path and files 
		if not bamfilename:
			bamfilename = os.path.splitext(self.basename)[0] + ".bam"		
		if outdir:
			bamfilename = os.path.join(outdir, bamfilename)			
		
		#prepare command & execute & clean
		if output.checkFilename(bamfilename):
			cmd = prog.split(" ") + ['view', '-@', str(cpus), '-b', self.absname, '-o', bamfilename]

			with open(logfile, 'a') as loghandle:
				subprocess.check_call(cmd, stderr=loghandle)

			if delSam:
				os.remove(self.absname)	
			
		#return obj
		return bam(bamfilename)
		

class bam(file):
	
	def __init__(self, filename, sorted = False, indexed = False, piledup = False, varcalled = False, silent=False):	
		super().__init__(filename)
		self.sorted = sorted
		self.indexed = indexed
		self.piledup = piledup
		self.varcalled = varcalled
		
	## SET/GET ATTRIBUTES ##
	def isSorted(self):
		return self.sorted

	def isIndexed(self):
		return self.indexed	
	
	def isPiledup(self):
		return self.piledup
		
	def isVarcalled(self):
		return self.varcalled
		
		
	## MODIFICATIONS ##
	def sort(self, filename = False, prog = 'samtools', logfile = "process.log", cpus = 1):
		'''
		sorts a bam file using samtools
		
		Parameter:
		filename		filename, if not given source file is overwritten [str]
		prog			name to call samtools [str]
		logfile			name of process logfile
		cpus			number of cpus
		
		Return:
		True
		
		Doctest:
		>>> bamfile = test.getTestBam()
		>>> logfile = os.path.join(test.getTestDir(), "process.log")	
		>>> obj = bam(bamfile)
		>>> obj.sort(logfile=logfile)
		True
		>>> test.cleanTestDir()
		
		'''	
		if not filename:
			filename = self.absname + ".tmp"
			rename = True
		else:
			rename = False
		
		if output.checkFilename(filename):
			cmd = prog.split(" ") + ['sort', '-@', str(cpus), '-o', filename, self.absname]
			with open(logfile, 'a') as loghandle:
				subprocess.check_call(cmd, stderr=loghandle)
			
			if rename:
				os.rename(filename,  filename[:-4])
				filename = filename[:-4]
		
		self.sorted = True
		self.rename(filename)
		
		return True
		
		
	def index(self, prog = "samtools", logfile = "process.log"):
		'''
		creates a bai index of a bam file
			
		Parameter:
		logfile		name of process logfile
			
		Return:
		True
		
		Doctest:
		
		>>> bamfile = test.getTestBam()
		>>> logfile = os.path.join(test.getTestDir(), "process.log")
		>>> obj = bam(bamfile)
		>>> obj.index(logfile=logfile)
		True
		>>> os.remove(obj.absname + ".bai")
							
		'''
		if not self.isSorted():
			self.sort()
			
		cmd = prog.split(" ") + ['index', '-b', self.absname]
		with open(logfile, 'a') as loghandle:
			subprocess.check_call(cmd, stderr=loghandle)
			
		self.indexed = True
		
		return True
		
	def delIndex(self):
		try:
			os.remove(self.absname + ".bai")
		except OSError:
			pass
			
		self.indexed = False
	
	def idxstats(self, prog = "samtools"):
		'''
		creates a bai index of a bam file
			
		Parameter:
		logfile		name of process logfile
			
		Return:
		idxstats output (list of lines)
		
		Doctest:
		tested in countMappedReads()
							
		'''
		
		if not self.sorted:
			self.sort()

		if not self.indexed:
			self.index()
			
			
		p = subprocess.Popen(prog.split(" ") + ['idxstats', self.absname], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		output, err = p.communicate()
		return output.decode('utf-8')	

	def countMappedReads(self, prog = "samtools"):
		'''
		Doctest
			>>> bamfile = test.getTestBam()
			>>> obj = bam(bamfile)
			>>> obj.countMappedReads()
			1128540
			>>> test.cleanTestDir()
			
		'''
		True	
		stats = self.idxstats().split("\n")
		return int(stats[0].split("\t")[2])
			
	def mpileup(self, reference, outdir, prog = "samtools", param = "-B", filename = False, logfile = "process.log", cpus = 1):
		'''
		generates a mpileup file using samtools
		
		Parameter:
		reference	indexed FASTA file containing reference sequence [file]
		prog		cmd to execute samtools [str]
		param		parameter for samtool's mpileup [str]
		filename	filename of mpileup file [str]
		path		path to save result files [str]
		tmpdir		path to store temporary data
		index		reference has to be indexed [True | False]
		logfilename	process logfile name
		cpus		number of cpus
		silent		show no messages except error messages
		
		Return:
		mpileup object [obj]
		
		Doctest:
		>>> bamfile = test.getTestBam()
		>>> reffile = test.getTestRef()
		>>> outdir = test.getTestDir()
		>>> logfile = os.path.join(test.getTestDir(), "process.log")	
		>>> obj = bam(bamfile)
		>>> mobj = obj.mpileup(reffile, outdir, logfile=logfile)
		>>> test.cleanTestDir()
					
		'''
		
		#prepare output file
		if not filename:
			filename = os.path.splitext(self.basename)[0] + ".pileup"			
		filename = os.path.join(outdir, filename)		
		output.checkFilename(filename)

		#prepare cmd & execute
		cmd = prog.split(" ") + ["mpileup", param, "-f", reference, "-o", filename, self.absname]
		with open(logfile, 'a') as loghandle:
			subprocess.check_call(cmd, stderr=loghandle)	
		
		return pileup(filename)
		
			
class pileup(file):
	'''
	pileup file object
	'''

	def __init__(self, filename, silent=False):	
		super().__init__(filename)
		self.refFreq = False
		
	def readPileup(self):
		with open(self.absname, "r") as handle:
			for line in handle:
				yield line
	
	def getRefFreq(self, pos):
		'''
			pos is 0-based!
		'''
		if self.refFreq is False:
			self.refFreq = {}
			for line in self.readPileup():
				fields = line.split("\t")
				p = int(fields[1]) - 1
				readcount = int(fields[3])
				if readcount == 0:
					self.refFreq[p] = 0
				else:	
					self.refFreq[p] = (fields[4].count(",") + fields[4].count("."))/readcount
		
		if not pos in self.refFreq:
			return 0
		else:
			return self.refFreq[pos]
		

	def callVar(self, outdir, mincov = 10, minreads2=8, minavgqual=20, minvarfreq=0.8, minhomfreq=0.8, strandfilter=1, pval=0.01, prog="varscan", filename = False, logfile = "process.log", cpus = 1):
		'''
		variant calling using varscan
		
		Parameter:
		mincov			corresponds to Varscan's --min-coverage parameter [int]
		minreads2		corresponds to Varscan's --min-reads2 parameter [int]
		minavgqual		corresponds to Varscan's --min-avg-qual parameter [int]
		minvarfreq		corresponds to Varscan's --min-var-freq parameter [float]
		minhomfreq		corresponds to Varscan's --min-freq-for-hom parameter [float]
		pval			corresponds to Varscan's --p-value parameter [float]
		strandfilter	corresponds to Varscan's --strand-filter parameter [1 | 0]
		prog			cmd to execute varscan
		filename		filename of vcf file [str]
		path			path to save result files [str]
		tmpdir			path to store temporary data [str]
		index			reference has to be indexed [True | False]
		logfile			process logfile name [str]
		cpus			number of cpus [int]
		silent			show no messages except error messages	[True | False]	
		
		Return:
		vcf object [obj] or False
		
		Doctest:
		>>> pileupfile = test.getTestPileup()
		>>> obj = pileup(pileupfile, silent=True)
		>>> outdir = test.getTestDir()
		>>> logfile = os.path.join(test.getTestDir(), "process.log")
		>>> vcfobj = obj.callVar(outdir, logfile=logfile)
		>>> test.cleanTestDir()
		
		'''		
				
		#prepare path and files
		if not filename:
			filename = os.path.splitext(self.basename)[0] + ".vcf"

		filename = os.path.join(outdir, filename)
		
		#prepare cmd & execute
		if output.checkFilename(filename):
			cmd = prog.split(" ")
			cmd.extend(["mpileup2cns", self.absname, "--output-vcf"])
			cmd.extend(['--min-coverage', mincov])
			cmd.extend(['--min-reads2', minreads2])
			cmd.extend(['--min-avg-qual', minavgqual])
			cmd.extend(['--min-var-freq', minvarfreq])
			cmd.extend(['--min-freq-for-hom', minhomfreq])
			cmd.extend(['--p-value', pval])
			cmd.extend(['--strand-filter', strandfilter])
			
			cmd = [str(x) for x in cmd]
			with open(filename, "w") as vcfhandle, open(logfile, 'a') as loghandle:
				subprocess.check_call(cmd, stdout=vcfhandle, stderr=loghandle)
			
			return vcf(filename)
			
		else:
			return False
		
	
class vcf(file):
	def __init__(self, filename, silent=False):	
		super().__init__(filename)
		self.called = False
		self.cns = False
						
	@staticmethod
	def readVCF(filename, spec=4.1, silent=False):
		'''
		Doctest:
		tested in cnsCollector.showAlign()
		
		'''	
		if not silent:
			print("processing", filename, "...")		
		
		with open(filename, 'r') as handle:		
			
			#VCF 4.1
			if spec == 4.1:
				#find header:
				for line in handle:
					line = line.strip()
									
					if line[0] == "#" and line[1] != "#":
						header = line[1:].split("\t")
						break
							
			
				#process data lines after header
				for line in handle:
					line = line.strip()
					if not line:
						continue 
					data={}
					data['LINE'] = line
					if not line:
						continue
						
					fields = line.split("\t")
					i = 0
					for field in fields:
						data[header[i]] = field
						i += 1
						
					#info string to dict
					info = data['INFO']
					data['INFO'] = {}
					for inf in info.split(";"):
						f = inf.split("=")
						data['INFO'][f[0]] = f[1]
					
					#format/sample string to dict
					formats = data['FORMAT'].split(":")
					sample = data['Sample1'].split(":")
					data['Sample1'] = {}
					l = len(sample)
					i = -1
					for format in formats:
						i += 1
						if i < l:
							data['Sample1'][format] = sample[i]
						else:
							data['Sample1'][format] = False

					yield data
			
	
	
	def createConsensi(self, reference, varfreq_excludewt = 0.2, minvarfreq=0.8, pvalthresh=0.01, warnthresh = 0.7, pileupfile = False, logfile = "consensus.log", report='critical', sample='Sample1', silent = False):
		'''
		mindreads3	minimal number of variant bases to exclude wild type call
		
		Doctest:
		tested in cnsCollector.showAlign()
		
		'''
		
		def reportCall(comment=False, mark =""):
			if data['ALT'] == ".":
				data['ALT'] = data['REF']
				if comment == False:
					comment = "called (ref)"
			elif comment == False:
					comment = "called (var)"
			return [mark, data['POS'], data['REF'], data['ALT'], data['FILTER'], data[sample]['FREQ'], data[sample]['RD'], data[sample]['AD'], data[sample]['RBQ'], data[sample]['ABQ'], data[sample]['PVAL'], data['INFO']['NC'], cns[pos], comment]	
		
		#pileup file processing
		if pileupfile:
			pobj = pileup(pileupfile)
		
		#preparing ref & cns / pval lists
		refs = fasta.toDict(reference, silent = True)		
		if len(refs) > 1:
			sys.exit("ERROR: So far Consensus Creator accepts only FASTA files containing exactly one reference sequence. Sorry!")
		elif len(refs) == 0:
			sys.exit("ERROR: FASTA file must contain at least one reference sequence.")
			
		
		pvals = {}
		for ref, refseq in refs.items():
			ref = ref[1:]
			refac = ref.split(" ")[0]
			cns = list('N' * len(refseq))
			pvals['calls'] = [20] * len(refseq)	
		
		#preparing params
		varfreq_excludewt = varfreq_excludewt * 100
		minvarfreq = minvarfreq * 100
		warnthresh = warnthresh * 100
		
		#prepare logging
		reportlog = [["mark", "position (reference-based)", "reference base", "consensus base", "filter", "variant frequency", "depth of reference-supporting bases", "depth of variant-supporting bases", "average quality of reference-supporting bases",  "average quality of variant-supporting bases", "p-value", "NC flag", "call", "comment"]]
		
		#consensus creation
		calls = 0
		for data in self.readVCF(self.absname, silent=silent):			
			#read parameter
			pos = int(data['POS']) - 1

			nc = int(data['INFO']['NC'])
						
			freq = data[sample]['FREQ']
			if freq:
				freq = float(freq[:-1].replace(",", "."))					
			
			pval = data[sample]['PVAL']
			if pval:
				pval = float(pval.replace(",", "."))
				
			if data['CHROM'] != refac:
				sys.exit("ERROR: VCF file contains data to unknown reference named " + refac)
			
			#NC filter [not called]
			if nc > 0:
				reportlog.append(reportCall("not called (NC flag)"))
				continue
				
			#filter filter
			if data['FILTER'] != "PASS":				
				reportlog.append(reportCall("not called (filter " + data['FILTER'] + ")"))
				continue				

			#missing value filter
			if freq is False or pval is False:				
				reportlog.append(reportCall("not called (unreliable)"))
				continue
			
			#pval and freq filter for variant sites
			if data['ALT'] != ".":
				#p-value filter
				if pval > pvalthresh:
					reportlog.append(reportCall("not called (p-value too high)"))
					continue
				
				#freq filter for variants
				if freq < minvarfreq:
					if freq >= warnthresh:
						mark = "!"
					else:
						mark = "" 
					reportlog.append(reportCall("not called (variant frequency too low)", mark))
					continue					
			
			#pval check and var noise filter for reference base calls
			if data['ALT'] == ".":
				#var noise filter
				if freq >= varfreq_excludewt:
					if pileupfile is False or pobj.getRefFreq(pos)*100 <= 100 - varfreq_excludewt:
						reportlog.append(reportCall("reference base not called (variant frequency too high)", "!"))
						continue

			#inferior call
			if pval > pvals['calls'][pos]:
				reportlog.append(reportCall("not called (inferior call)", "!"))
				continue				
						
			#unkown base call
			if len(data['ALT'].replace(".", "").replace("A", "").replace("T", "").replace("G", "").replace("C", "").replace("N", "")) > 0:
				reportlog.append(reportCall("unknown call"), "!")
				continue	

			#competing  call
			if pval == pvals['calls'][pos]:
				reportlog.append(reportCall("not called (competing  call)", "!"))
				continue
	

			#consider
			rlen = len(data['REF']) #reference length at position
			vlen = len(data['ALT'])#variant length at position
			
			#consider base call
			call = ""
			if rlen == vlen:
				if data['ALT'] == ".":
					cns[pos] = data['REF']
					call =  "reference base"
				else:
					cns[pos] = data['ALT']
					call =  "snp"
					
				if pval < pvals['calls'][pos] and pvals['calls'][pos] != 20 and cns[pos] != "N": 
					reportlog.append(reportCall( call + " called (superior call)", "!"))
				else:
					reportlog.append(reportCall( call + " called", ""))					
				pvals['calls'][pos] = pval
														
			#consider insert 
			elif rlen == 1 and vlen > 1:	
				cns[pos] = data['ALT']
				call = "insert and anchor base"				
					
				if pval < pvals['calls'][pos] and pvals['calls'][pos] != 20 and cns[pos] != "N": 
					reportlog.append(reportCall( call + " called (superior call)", "!"))
				else:
					reportlog.append(reportCall( call + " called", ""))	
				pvals['calls'][pos] = pval					
				
			#consider gap
			elif rlen > 1 and vlen == 1:
				g = 0
				for nuc in data['REF']:
					if g == 0:
						pvals['calls'][pos] = pval
						cns[pos] = nuc
						call = "anchor base"	
						g += 1
					else:
						cns[pos] = "-"
						call = "deletion"
							
					if pval < pvals['calls'][pos] and pvals['calls'][pos] != 20 and cns[pos] != "N": 
						reportlog.append(reportCall( call + " called (superior call)", "!"))
					else:
						reportlog.append(reportCall( call + " called", ""))	
						
					pvals['calls'][pos] = pval
					pos += 1
										
			else:
				reportlog.append(reportCall("unexpected", "!"))
		
		
		#logging
		if logfile:
			if report == 'critical':				
				pos = set([x[1] for x in reportlog if x[0] == "!"])
				reportlog = [reportlog[0]] + sorted([x for x in reportlog[1:] if x[1] in pos], key=lambda x: x[1]) 
			elif report == 'variants':
				pos = set([x[1] for x in reportlog if x[0] == "!"])
				reportlog = [reportlog[0]] + sorted([x for x in reportlog[1:] if x[2] != x[3] or x[1] in pos], key=lambda x: x[1]) 
			elif report == 'notref':
				pos = set([x[1] for x in reportlog if x[0] == "!"])
				reportlog = [reportlog[0]] + sorted([x for x in reportlog[1:] if x[2] != x[-2] or x[1] in pos], key=lambda x: x[1]) 				
			if report and len(reportlog) > 1:
				with output.createFile(logfile, silent = True) as handle:
					handle.write("\n".join(["\t".join([str(y) for y in x]) for x in reportlog]) )
				
		#cns to collection
		return self.basename, cns, refseq, ref, reference, pvals['calls']	
	

class cnsCollector():
	def __init__(self):	
		self.cns = []
		self.refseq = False
		self.refheader = False
		self.reffilename = False
		self.alignment = False
		self.stats = {}
	
	def add(self, vcfname, cns_nuclist, refseq, refheader, reffile, pval_bases):
		''' 
		Doctest:
		tested in cnsCollector.showAlign()
		
		'''
		
		if self.refseq != False:
			if vcfname in [x[0] for x in self.cns]:
				return False
			elif (refseq, refheader, reffile) != (self.refseq, self.refheader, self.reffilename):
				return False
			elif len(cns_nuclist) != len(self.refseq):
				return False
		else:
			self.refseq = refseq
			self.refheader = refheader
			self.reffilename = reffile
			
		self.cns.append([vcfname, cns_nuclist, pval_bases])
		return True	
		
			
	def getRefHeader(self):
		return self.refheader
	
	def getRefSeq(self):
		return self.refseq

	def getReffilename(self):
		return self.reffilename
	
	def align(self, prog = "mafft", cpus = 1, tmpdir = "tmp", logfile = "process.log", inserts=False):
		'''		
		Doctest:
		tested in cnsCollector.showAlign()
		
		'''
		
		if os.path.exists(tmpdir) is False:
			tmpdir = os.path.abspath(output.createDir(tmpdir, silent = True))
			
		seqlists = [list(self.refseq)] + [x[1] for x in self.cns]
		header = [self.refheader] + [x[0] for x in self.cns]
		algn = [[] for x in seqlists]		

		#consider inserts
		if inserts:
			algn_inputfile = os.path.join(tmpdir, "algn.fasta")
			for nucs in zip(*seqlists):
				maxlen = max([len(x) for x in nucs])
				
				#nothing to align
				if maxlen == 1:
					i = -1
					for nuc in nucs:
						i += 1
						algn[i].append(nuc)				
					
				elif maxlen == 2 or len(set([1 for x in nucs if len(x) > 1])) <= 1:
					i = -1
					for nuc in nucs:
						i += 1
						algn[i].append(nuc + ("-" * (maxlen - len(nuc))))
				
				#align
				else:
					with open(algn_inputfile, "w") as outhandle:
						i = -1
						for nuc in nucs:
							i += 1
							algn[i].append(nuc[0])
							if len(nucs) > 1:
								outhandle.write(">" + str(i) + "\n" + nuc[1:] + "\n")
							
					mafftresult = fasta.mafft(algn_inputfile, prog=prog, cpus = cpus, logfile = logfile, silent=True)
					
					for line in mafftresult:
						if line[0] == ">":
							key = int(line[1:].strip())
							continue
						algn[key][-1] += line.strip()							
					maxlen = max([len(x) for x in algn])	
				
					for i in range(len(algn)):
						algn[i][-1] = algn[i][-1] + "-" * (maxlen - len(algn[i][-1]))
		
		#exclude inserts
		else:
			for nucs in zip(*seqlists):
				i = -1
				for nuc in nucs:
					i += 1		
					algn[i].append(nuc[0])	

		
		#create header-based dict
		out = {}
		i = 0
		for head in header:
			if head in out:
				sys.exit("ERROR: Sequence header not unique")
			out[head] = algn[i]
			i += 1
			
		self.alignment = out
		return True
		
	def showAlign(self, format="dict", inserts=False):
		'''
		Doctest:
		>>> vcffile = test.getTestVcf()
		>>> reffile = test.getTestRef()
		>>> logfile = os.path.join(test.getTestDir(), "consensus.log")
		>>> tmpdir = os.path.join(test.getTestDir(), "tmp")
		>>> obj = vcf(vcffile)
		>>> cnscollector = cnsCollector()
		>>> cnscollector.add(*obj.createConsensi(reffile, logfile=logfile, minvarfreq=0.6, silent=True))
		True
		>>> len(cnscollector.getCns('test.vcf'))
		2821351
		>>> cnscollector.align(tmpdir = tmpdir, logfile=logfile, inserts=True)
		True
		>>> aligned_seqs = cnscollector.showAlign(inserts=True)
		>>> len(aligned_seqs)
		2
		>>> len(aligned_seqs['NC_007795.1'])
		2821371
		>>> len(aligned_seqs['test.vcf'])
		2821371
		>>> for insert in cnscollector.iterInserts('test.vcf'):
		...  print(insert)
		('TTCGAGCTAC', 'TTCGAGCTAC', 2494899, 2494889)
		>>> test.cleanTestDir()

		'''
		if self.alignment == False:
			sys.exit("ERROR: Alignment built not yet. (use alignSeq)")

		out = {}
		for head, seqlist in self.alignment.items():
			if inserts:
				out[head] = "".join(seqlist)						
			else:
				out[head] = "".join([x[0] for x in seqlist])
			
		if format == "dict":
			return out
		
		elif format == "fasta":			
			fasta = []
			for header, seq  in out.items():
				if self.refheader == header:
					fasta.insert(0, ">" + header + "\n" + seq)	
					continue
					
				if header.endswith(".vcf"):
					header = header[:-4]	
				elif header.endswith(".vcf.gz"):
					header = header[:-7]
				fasta.append(">" + header + "\n" + seq)		
				
			return "\n".join(fasta)
			
	def iterHeader(self):
		for data in self.cns:
			yield data[0]
			
	def getCns(self, header, inserts=False):
		if header not in [x[0] for x in self.cns]:
			sys.exit("ERROR: header '" + header + "' not found in collection")
		
		for data in self.cns:
			if data[0] == header:
				if inserts:
					return "".join(data[1]).replace("-", "")
				else:
					return "".join([x[0] for x in data[1]]).replace("-", "")
	
	def iterInserts(self, header):
		'''
		Doctest:
		tested in cnsCollector.showAlign()
		
		'''			
		if self.alignment == False:
			sys.exit("ERROR: collection not yet aligned!")
			
		i = 0
		j = 0
		for nuc in self.alignment[header]:
			ungappy_len = len(nuc.replace("-", ""))
			i += len(nuc)
			j += ungappy_len
			
			if ungappy_len > 1:
				yield (nuc[1:], nuc[1:].replace("-", ""), i, j)
				
	def getStat(self, header):
		'''
		Doctest:
		tested in cnsCollector.showAlign()
		
		'''	
		if self.alignment == False:
			sys.exit("ERROR: collection not yet aligned!")
			
		if header not in self.stats:
			pos = -1
			j = 0
			ref_bases = 0
			snps = 0
			deleted_bases = 0
			gaps = 0
			inserts = 0
			inserted_bases = 0
			n = 0
			for nuc in self.alignment[header]:
				pos += 1
				refnuc = self.alignment[self.refheader][pos]
				if nuc == "N":
					n += 1
				elif nuc == "-" and refnuc == "-":
					continue
				elif nuc == "-":
					deleted_bases += 1
					if not self.alignment[header][pos-1] == "-":
						gaps += 1
				elif refnuc == "-":
					inserted_bases += 1
					if not self.alignment[self.refheader][pos-1] == "-":
						inserts += 1
				elif not nuc == refnuc:
					snps += 1
				else:
					ref_bases += 1
			
			length = ref_bases + snps + inserted_bases + n
			
			self.stats[header] = {}	
			self.stats[header]['ref_bases'] = ref_bases	
			self.stats[header]['snps'] = snps	
			self.stats[header]['deleted_bases'] = deleted_bases	
			self.stats[header]['gaps'] = gaps	
			self.stats[header]['inserts'] = inserts	
			self.stats[header]['inserted_bases'] = inserted_bases	
			self.stats[header]['length'] = length
			self.stats[header]['N'] = n
		
		return self.stats[header]
		
# MAIN #	
if __name__ == "__main__":
	import doctest
	print("version " + version)
	if os.path.isdir(test.getTestDir()):
		shutil.rmtree(test.getTestDir())
	print("doctest ...")
	print("creating test environment in folder", test.getTestDir(), "...")
	output.createDir(test.getTestDir(), silent=True)
	#doctest.run_docstring_examples(cnsCollector.showAlign, globals())
	doctest.testmod()
	shutil.rmtree(test.getTestDir())
	print("deleting test environment", "...")
	
		
	